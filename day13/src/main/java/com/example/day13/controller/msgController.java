package com.example.day13.controller;

import com.example.day13.services.IMessageService;
import com.example.day13.dto.msgDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class msgController {
    @Autowired
    IMessageService msgService;

    @GetMapping("/v1/messages")
    public ResponseEntity<msgDTO> getMessage(){
        msgDTO dto = msgService.getMessage();
        return new ResponseEntity<>(dto, HttpStatus.OK);
    };
    @PostMapping("v1/messages")
    public ResponseEntity<msgDTO> postMessage(){
        msgDTO dto = msgService.postMessage();
        return new ResponseEntity<>(dto, HttpStatus.OK);
    };
    @DeleteMapping("v1/messages")
    public ResponseEntity<msgDTO> deleteMessage(){
        msgDTO dto = msgService.deleteMessage();
        return new ResponseEntity<>(dto, HttpStatus.OK);
    };
}
