package com.example.day13.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class msgDTO {
    @JsonProperty("Response Code")
  private HttpStatus responseCode;
    @JsonProperty("Message")
  private String msg;
    @JsonProperty("Data")
  private String data;
}
