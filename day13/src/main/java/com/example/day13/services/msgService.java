package com.example.day13.services;


import com.example.day13.dto.msgDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


@Service
public class msgService implements IMessageService{

    @Override
    public msgDTO getMessage() {
        msgDTO dto = new msgDTO();
        dto.setResponseCode(HttpStatus.OK);
        dto.setMsg("Get Success");
        dto.setData("Hello World");
        return dto;
    }

    @Override
    public msgDTO postMessage() {
        msgDTO dto = new msgDTO();
        dto.setResponseCode(HttpStatus.OK);
        dto.setMsg("Post Success");
        dto.setData("Hello World");
        return dto;
    }

    @Override
    public msgDTO deleteMessage() {
        msgDTO dto = new msgDTO();
        dto.setResponseCode(HttpStatus.OK);
        dto.setMsg("Delete Success");
        dto.setData("Hello World");
        return dto;
    }
}
