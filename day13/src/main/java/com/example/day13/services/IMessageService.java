package com.example.day13.services;


import com.example.day13.dto.msgDTO;

public interface IMessageService {
msgDTO getMessage();
msgDTO postMessage();
msgDTO deleteMessage();
}
