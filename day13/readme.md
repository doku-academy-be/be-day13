#GET Message
untuk mengambil data yang berisi: 
    -Response Code dengan nilai "OK" 
    -Message dengan nilai "Get Success"
    -Data dengan nilai "Hello World"
dari lokal server yaitu localhost:8080


#POST Message
Fungsi POST sebenernya untuk menambahkan data ke server
tetapi di aplikasi ini hanya menampilkan data yang berisi:
    -Response Code dengan nilai "OK"
    -Message dengan nilai "Post Success"
    -Data dengan nilai "Hello World"
dari lokal server yaitu localhost:8080

#DELETE Message
Fungsi dari DELETE untuk menghapus data yang ada di server
tetapi di aplikasi ini hanya menampilkan data yang berisi:
    -Response Code dengan nilai "OK"
    -Message dengan nilai "Delete Success"
    -Data dengan nilai "Hello World"
dari lokal server yaitu localhost:8080